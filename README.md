<h1 style="text-align: left;">一、框架的由来&nbsp; 快速入门</h1>
<table width="1280">
<tbody>
<tr>
<td>
<h1><a title="PDF.NET SOD 官网" href="http://www.pwmis.com/sqlmap" target="_blank">有关框架的更多信息，请看框架官方主页！</a></h1>
<p>本套框架的思想是借鉴Java平台的Hibernate 和 iBatis 而来，兼有ORM和SQL-MAP的特性，同时还参考了后来.NET的LINQ（本框架成型于2006年，当时还未听说过LINQ）使用风格，设计了OQL查询表达式。本框架的设计思想是通用的，完全可以移植到Java 平台，现在只提供了.NET平台的实现，暂且将本框架命名为</p>
<h1><span style="color: #00ffff;">PDF.NET</span></h1>
</td>
</tr>
<tr>
<td colspan="2">
<p>从2013.10.1日起，原PDF.NET 将更名为</p>
<h1>SOD</h1>
<h2>one SQL-MAP,ORM,Data Control framework</h2>
<p><br /> 原PDF.NET框架将成为一个全功能的企业开发框架，而 SOD框架将是PDF.NET开发框架下面的 &ldquo;数据开发框架"</p>
<p><img src="http://www.pwmis.com/sqlmap/pdfnet_fwk2.png" alt="PDF.NET" width="588" height="543" /></p>
<p><span style="font-size: 18pt;">PDF.NET 开源历史：</span></p>
<ol>
<li>2010.2--PDF.NET3.0 会员发布版</li>
<li>2010.5--PDF.NET3.5 会员发布版</li>
<li>2011.3--PDF.NET4.0 会员发布版</li>
<li>2011.9--PDF.NET Ver 3.0 开源版&nbsp;&nbsp;&nbsp; 节前送礼：PDF.NET（PWMIS数据开发框架）V3.0版开源&nbsp;&nbsp;&nbsp; <a href="http://www.cnblogs.com/bluedoctor/archive/2011/09/29/2195751.html">http://www.cnblogs.com/bluedoctor/archive/2011/09/29/2195751.html</a></li>
<li>2012.9--PDF.NET Ver 4.5 开源版 <a id="cb_post_title_url" href="http://www.cnblogs.com/bluedoctor/archive/2012/09/28/2707093.html"> 节前送礼：PDF.NET（PWMIS数据开发框架）V4.5版开源</a></li>
<li>2014.1--PDF.NET Ver 5.1 <a id="cb_post_title_url" href="http://www.cnblogs.com/bluedoctor/p/3535850.html"> 春节前最后一篇，CRUD码农专用福利：PDF.NET之SOD Version 5.1.0 开源发布（兼更名）</a></li>
<li>2015.2--PDF.NET SOD Ver5.1 <a id="cb_post_title_url" href="http://www.cnblogs.com/bluedoctor/p/4273610.html"> 一年之计在于春，2015开篇：PDF.NET SOD Ver 5.1完全开源</a>&nbsp;</li>
</ol>
<p>&nbsp;</p>
<p><span style="font-size: 18pt;">开源协议：</span></p>
<ul>
<li>框架类库开源协议：采用LGPL协议，<span style="color: #3366ff;">该协议允许商业使用，但仅限于包含类库发布，不得将源码作为商业行为销售分发，详情请看该协议的官方说明。</span></li>
<li>框架支持工具开源协议：采用GPL协议，<span style="color: #ff6600;">不可用于商业销售分发和修改，如果你想用于商业用途或者闭源使用，请单独购买许可，详情请看该协议官方说明。</span></li>
<li>框架相关示例Demo开源协议：采用MIT协议，<span style="color: #008000;">可自由修改使用，详情请看该协议官方说明。</span></li>
</ul>
<p>注：框架的支持工具指的是集成开发工具，可以连接各种数据库进行查询，生成实体类，SQL-MAP DAL和 SqlMap.config 文件。</p>
</td>
</tr>
</tbody>
</table>
<table width="1283"><caption>
<h1 align="left">二、开源捐助账号</h1>
</caption>
<tbody>
<tr>
<td width="1281">
<p><a href="https://shenghuo.alipay.com/send/payment/fill.htm?_tosheet=true&amp;_pdType=afcabecbafgggffdhjch" target="_blank"><img src="http://www.pwmis.com/sqlmap/pdfnet-jk.png" alt="" /></a><img src="http://www.pwmis.com/sqlmap/alipay-jk.png" alt="" /></p>
<p>右图为二维码捐款方式</p>
<p>感谢所有ＰＤＦ．ＮＥＴ　框架的会员朋友热心的捐助，并为框架不断完善和推广作出的杰出贡献！2015新春之际，送红包给大家！</p>
<p><a href="http://download-codeplex.sec.s-msft.com/Download?ProjectName=pwmis&amp;DownloadId=1428886"><img style="padding-top: 0px; padding-left: 0px; display: inline; padding-right: 0px; border-width: 0px;" title="hongbao" src="http://download-codeplex.sec.s-msft.com/Download?ProjectName=pwmis&amp;DownloadId=1428887" alt="hongbao" width="725" height="504" border="0" /></a></p>
</td>
</tr>
</tbody>
</table>
<h1>三、快速入门：</h1>
<h2>3.1，总览</h2>
<p>SOD框架分为3大部分：</p>
<ol>
<li>SQL-MAP</li>
<li>ORM</li>
<li>Data Control</li>
</ol>
<p>&nbsp;</p>
<h2>3.2，&ldquo;SqlHelper&rdquo;基础</h2>
<p>这三大部分，都是基于AdoHelper组件，它符合MS DAAB标准，所以熟悉SqlHelper的人应该很容易上手，下面举例说明：</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;">
<pre>AdoHelper helper=<span style="color: #0000ff;">new</span><span style="color: #000000;"> SqlServer();
DataSet ds</span>=helper.ExecuteDataSet( &rdquo;Data Source=.;Initial Catalog=LocalDB;Integrated Security=<span style="color: #000000;">True&rdquo;,
      CommandType.Text,
      &rdquo;SELECT </span>* FROM Table_User&rdquo;);</pre>
</div>
<p>&nbsp;</p>
<p>AdoHelper是一个抽象类，所以它可以实例化成其他数据库访问类，比如继续下面的代码：</p>
<p>&nbsp;</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;">
<pre>helper=<span style="color: #0000ff;">new</span> Access(); <span style="color: #008000;">//</span><span style="color: #008000;">Access数据库访问类</span>
DataSet dsAcc=helper.ExecuteDataSet( &rdquo;Provider=Microsoft.ACE.OLEDB.<span style="color: #800080;">12.0</span>;Jet OLEDB:Engine Type=<span style="color: #800080;">6</span>;Data Source=<span style="color: #000000;">D:\Data\SuperMarket.accdb&rdquo;,
        CommandType.Text,
        &rdquo;SELECT </span>* FROM Table_User&rdquo;);</pre>
</div>
<p>在 PWMIS.Core.dll SOD核心库中，内置了SqlServer，SqlServerCe，Access，Oracle，OleDb，Odbc 等常见的数据库访问类提供程序。</p>
<p>&nbsp;</p>
<p>在程序中每次都指定连接字符串和查询命令类型，好处是&ldquo;随用随取&rdquo;，线程安全，随时随地&ldquo;SqlHelper&rdquo;，但不好之处就是代码冗余，所以可以把数据访问类类型和连接字符串放到应用程序配置文件中（App.config / Web.config ）：</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;">
<pre><span style="color: #0000ff;">&lt;</span><span style="color: #800000;">connectionStrings</span><span style="color: #0000ff;">&gt;</span>
    <span style="color: #0000ff;">&lt;</span><span style="color: #800000;">add </span><span style="color: #ff0000;">name</span><span style="color: #0000ff;">="AccessDb"</span> </pre>
<pre><span style="color: #ff0000;">  connectionString</span><span style="color: #0000ff;">="Provider=Microsoft.ACE.OLEDB.12.0;Jet OLEDB:Engine Type=6;Data Source= |DataDirectory|SuperMarket.accdb"</span> </pre>
<pre><span style="color: #ff0000;">  providerName</span><span style="color: #0000ff;">="Access"</span><span style="color: #0000ff;">/&gt;</span>
 <span style="color: #0000ff;">&lt;/</span><span style="color: #800000;">connectionStrings</span><span style="color: #0000ff;">&gt;</span></pre>
</div>
<p>在上面的连接配置中， <span style="color: #ff0000;">providerName</span><span style="color: #0000ff;">="Access"</span>&nbsp;&nbsp;&nbsp; 表示这将是用SOD框架的Access数据库访问提供程序，同样道理，如果 <span style="color: #ff0000;">providerName</span><span style="color: #0000ff;">="SqlServer"</span>&nbsp; 将使用SqlServer提供程序。</p>
<p>如果是SOD 核心程序之外的数据访问提供程序，需要使用下面格式的连接配置：</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;">
<pre><span style="color: #0000ff;">&lt;</span><span style="color: #800000;">add </span><span style="color: #ff0000;">name</span><span style="color: #0000ff;">="default"</span><span style="color: #ff0000;"> 
  connectionString</span><span style="color: #0000ff;">="server=10.0.0.1;User Id=pdfnet;password=pdfnet2015;CharSet=utf8;DataBase=test;Allow Zero Datetime=True"</span><span style="color: #ff0000;"> 
providerName</span><span style="color: #0000ff;">="PWMIS.DataProvider.Data.MySQL,PWMIS.MySqlClient"</span><span style="color: #0000ff;">/&gt;</span></pre>
</div>
<p>这个配置说明，连接名为 default 的SOD数据访问驱动程序 所在程序集 是 PWMIS.MySqlClient.dll ,提供程序全名称是 PWMIS.DataProvider.Data.MySQL 。</p>
<p><font color="#0000ff">注意：Web.config 文件中，连接字符串支持|DataDirectory| 路径格式。</font></p>
<p>配置了数据库连接信息之后，就可以在程序中像下面这样使用了：</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;">
<pre><span style="color: #0000ff;">using</span> PWMIS.DataProvider.Adapter;<span style="color: #008000;">//</span><span style="color: #008000;">&hellip;</span>
AdoHelper accessDb1=MyDB.Instance;<span style="color: #008000;">//</span><span style="color: #008000;">应用程序配置文件连接配置节的最后一个数据连接配置</span>
AdoHelper accessDb2=MyDB.GetDBHelperByConnectionName(&ldquo;AccessDb&rdquo;); <span style="color: #008000;">//</span><span style="color: #008000;">连接字符串名字</span></pre>
<pre>AdoHelper mySqlDb=MyDB.GetDBHelperByConnectionName(&ldquo;default&rdquo;); <span style="color: #008000;">//</span><span style="color: #008000;">连接字符串名字</span> 
<span style="color: #0000ff;">bool</span> flag= accessDb.GetType() == <span style="color: #0000ff;">typeof</span>(Access); <span style="color: #008000;">//</span><span style="color: #008000;">flag=true;</span></pre>
<pre><span style="color: #0000ff;">bool</span> flag2= mySqlDb.GetType() == <span style="color: #0000ff;">typeof</span>(PWMIS.DataProvider.Data.MySQL); <span style="color: #008000;">//</span><span style="color: #008000;">flag2=true;</span></pre>
</div>
<p>注意示例中的 MyDB.Instance 对象，这是系统使用的默认数据访问类，它始终读取的是应用程序配置文件连接配置节的最后一个数据连接配置，这是一个静态单利对象，请勿在事务中使用它，初此之外，在任何地方使用它都是可以的，但仍然不建议你在多线程环境下使用 MyDB.Instance 这个AdoHelper的单例对象，推荐 accessDb2 的AdoHeper 实例化方式。</p>
<p>&nbsp;</p>
<h2><strong>3.3，微型ORM</strong></h2>
<p>&nbsp;</p>
<p>除此之外，AdoHelper 对象还是一个&ldquo;<strong>微型ORM</strong>&rdquo;，请看下面的示例：</p>
<div>
<pre>AdoHelper dbLocal = <span style="color: #0000ff;">new</span><span style="color: #000000;"> SqlServer();
dbLocal.ConnectionString </span>= <span style="color: #800000;">"</span><span style="color: #800000;">Data Source=.;Initial Catalog=LocalDB;Integrated Security=True</span><span style="color: #800000;">"</span><span style="color: #000000;">;
</span><span style="color: #0000ff;">var</span> dataList = dbLocal.GetList(reader =&gt;<span style="color: #000000;">
{
    </span><span style="color: #0000ff;">return</span> <span style="color: #0000ff;">new</span><span style="color: #000000;">
   {
       UID</span>=reader.GetInt32(<span style="color: #800080;">0</span><span style="color: #000000;">),
       Name</span>=reader.GetString(<span style="color: #800080;">1</span><span style="color: #000000;">)
   };
}, </span><span style="color: #800000;">"</span><span style="color: #800000;">SELECT UID,Name FROM Table_User WHERE Sex={0} And Height&gt;={1:5.2}</span><span style="color: #800000;">"</span>,<span style="color: #800080;">1</span>, <span style="color: #800080;">1.60M</span>);</pre>
</div>
<p>上面将一条SQL语句的结果，直接映射到了一个匿名实体类上，注意还有格式化参数的功能。</p>
<p>如果不想接SQL结果映射到匿名类型上,而是一个结构根SQL结果类型相同的POCO类上,可以这样使用:</p>
<div>
<pre><span style="color: #008000;">//</span><span style="color: #008000;">假设UserPoco 对象跟 Table_User 表是映射的相同结构</span>
AdoHelper dbLocal = <span style="color: #0000ff;">new</span><span style="color: #000000;"> SqlServer();
dbLocal.ConnectionString </span>= <span style="color: #800000;">"</span><span style="color: #800000;">Data Source=.;Initial Catalog=LocalDB;Integrated Security=True</span><span style="color: #800000;">"</span><span style="color: #000000;">;
</span><span style="color: #0000ff;">var</span> list=dbLoal.QueryList&lt;UserPoco&gt;(<span style="color: #800000;">"</span><span style="color: #800000;">SELECT UID,Name FROM Table_User WHERE Sex={0} And Height&gt;={1:5.2}</span><span style="color: #800000;">"</span>,<span style="color: #800080;">1</span>, <span style="color: #800080;">1.60M</span>);</pre>
</div>
<p>该功能可以类似流行的Dapper 数据访问组件,可以使用SOD AdoHelper替代使用。</p>
<p>&nbsp;</p>
<h2>3.4，小结：</h2>
<p>上面说明了PDF.NET SOD框架最基础的数据访问组件 AdoHelper 的使用，但这也是很多新手朋友不太清楚的地方，特别是 MyDB.Instance 单例对象容易滥用，一定要掌握。</p>
<p>SOD框架绝大部分情况下，只需要进行上面的数据连接配置，即可顺利运行代码了，比起Entity Framework 的配置来，是不是简单很多？</p>
<p>看到这里，我想你应该入门了，下面就让我们简要的浏览下SOD框架的一个大概。</p>
<p>----</p>
<h1>四、SQL-MAP</h1>
<p>你是不是曾经或者看到过别人做的项目，</p>
<ul>
<li>大量拼接SQL语句，或者SQL参数化查询后拖沓呈长而又繁琐的代码，甚至SQL满天飞？</li>
<li>维护这样的代码是不是非常痛苦？</li>
<li>每天写这样的CRUD代码是不是感觉有点浪费生命？</li>
</ul>
<p>现在好了，你可以将所有SQL语句集中写到一个配置文件中，集中管理维护你的查询程序，甚至，这个工作你可以丢给DBA去做！</p>
<p>本功能类似于Java界著名的 iBatis 和移植到.Net的 MyBatis.Net ，但是，SQL-MAP去除了它们沉长的配置，并且使用工具自动生成代码，使得<font color="#ff0000">编写DAL 数据访问层不需要写一行代码</font>，请参考下面的文章：</p>
<p><a href="http://www.pwmis.com/sqlmap/toolsHelp.htm" target="_blank">《PDF.NET 之SQL-MAP 使用图解教程》</a></p>
<p>更多的内容，你也可以参考SOD框架官方博客的介绍：</p>
<p><a href="http://www.cnblogs.com/bluedoctor/archive/2011/08/18/2144826.html">（PDF.NET框架实例讲解）将存储过程映射为实体类</a> 深蓝医生 2011-08-18 17:25 阅读:1748 评论:6</p>
<p><a href="http://www.cnblogs.com/bluedoctor/archive/2011/05/13/2045376.html">使用XSD编写具有智能提示的XML文件（以SQL-MAP脚本为实例）</a> 深蓝医生 2011-05-13 12:17 阅读:1609 评论:7</p>
<p><a href="http://www.cnblogs.com/bluedoctor/archive/2011/05/07/2039527.html">PDF.NET（PWMIS数据开发框架）之SQL-MAP目标和规范</a> 深蓝医生 2011-05-07 00:05 阅读:937 评论:1</p>
<p><a href="http://www.cnblogs.com/bluedoctor/archive/2011/05/06/2038727.html">抽象SQL查询：SQL-MAP技术的使用</a> 深蓝医生 2011-05-06 11:59 阅读:3598 评论:21</p>
<p><a href="http://www.cnblogs.com/bluedoctor/archive/2011/02/25/1965283.html">使用OQL+SQLMAP解决ORM多表复杂的查询问题</a> 深蓝医生 2011-02-25 19:08 阅读:928 评论:0</p>
<p><a href="http://www.cnblogs.com/bluedoctor/archive/2010/07/03/1769890.html">PDF.NET数据开发框架 之SQL-MAP使用存储过程</a> 深蓝医生 2010-07-03 23:31 阅读:2386 评论:4</p>
<p>&nbsp;</p>
<h1>五、ORM</h1>
<p>SOD框架发明了独具特色的ORM查询语言OQL，它基本覆盖了SQL92标准的大部分功能，使得你在VS IDE 使用&ldquo;对象化的SQL&rdquo;。目前做到这个功能的除了Linq之外，没有更多的ORM具有这个能力，但是对比EF框架的查询语言Linq，OQL有自己的特色，它跟SQL更为接近，对.NET框架的依赖非常小，这使得你有可能将OQL移植到Java ，C++ 这样的面向对象的语言。</p>
<p>下面给出一个简单的示例，有关该示例的详细内容，请参考这篇博客文章《<a href="http://www.cnblogs.com/bluedoctor/p/4286707.html">DataSet的灵活,实体类的方便，DTO的效率：SOD框架的数据容器，打造最适合DDD的ORM框架</a>》</p>
<p>SOD的实体类示例：</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;">
<pre><span style="color: #0000ff;">public</span> <span style="color: #0000ff;">class</span><span style="color: #000000;"> UserEntity:EntityBase, IUser
    {
        </span><span style="color: #0000ff;">public</span><span style="color: #000000;"> UserEntity()
        {
            TableName </span>= <span style="color: #800000;">"</span><span style="color: #800000;">Users</span><span style="color: #800000;">"</span><span style="color: #000000;">;
            IdentityName </span>= <span style="color: #800000;">"</span><span style="color: #800000;">User ID</span><span style="color: #800000;">"</span><span style="color: #000000;">;
            PrimaryKeys.Add(</span><span style="color: #800000;">"</span><span style="color: #800000;">User ID</span><span style="color: #800000;">"</span><span style="color: #000000;">);
        }

        </span><span style="color: #0000ff;">public</span> <span style="color: #0000ff;">int</span><span style="color: #000000;"> UserID
        {
            </span><span style="color: #0000ff;">get</span> { <span style="color: #0000ff;">return</span> getProperty&lt;<span style="color: #0000ff;">int</span>&gt;(<span style="color: #800000;">"</span><span style="color: #800000;">User ID</span><span style="color: #800000;">"</span><span style="color: #000000;">); }
            </span><span style="color: #0000ff;">set</span> { setProperty(<span style="color: #800000;">"</span><span style="color: #800000;">User ID</span><span style="color: #800000;">"</span><span style="color: #000000;">, value); }
        }

        </span><span style="color: #0000ff;">public</span> <span style="color: #0000ff;">string</span><span style="color: #000000;"> FirstName
        {
            </span><span style="color: #0000ff;">get</span> { <span style="color: #0000ff;">return</span> getProperty&lt;<span style="color: #0000ff;">string</span>&gt;(<span style="color: #800000;">"</span><span style="color: #800000;">First Name</span><span style="color: #800000;">"</span><span style="color: #000000;">); }
            </span><span style="color: #0000ff;">set</span> { setProperty(<span style="color: #800000;">"</span><span style="color: #800000;">First Name</span><span style="color: #800000;">"</span>, value,<span style="color: #800080;">20</span><span style="color: #000000;">); }
        }

        </span><span style="color: #0000ff;">public</span> <span style="color: #0000ff;">string</span><span style="color: #000000;"> LasttName
        {
            </span><span style="color: #0000ff;">get</span> { <span style="color: #0000ff;">return</span> getProperty&lt;<span style="color: #0000ff;">string</span>&gt;(<span style="color: #800000;">"</span><span style="color: #800000;">Last Name</span><span style="color: #800000;">"</span><span style="color: #000000;">); }
            </span><span style="color: #0000ff;">set</span> { setProperty(<span style="color: #800000;">"</span><span style="color: #800000;">Last Name</span><span style="color: #800000;">"</span>, value,<span style="color: #800080;">10</span><span style="color: #000000;">); }
        }

        </span><span style="color: #0000ff;">public</span> <span style="color: #0000ff;">int</span><span style="color: #000000;"> Age
        {
            </span><span style="color: #0000ff;">get</span> { <span style="color: #0000ff;">return</span> getProperty&lt;<span style="color: #0000ff;">int</span>&gt;(<span style="color: #800000;">"</span><span style="color: #800000;">Age</span><span style="color: #800000;">"</span><span style="color: #000000;">); }
            </span><span style="color: #0000ff;">set</span> { setProperty(<span style="color: #800000;">"</span><span style="color: #800000;">Age</span><span style="color: #800000;">"</span><span style="color: #000000;">, value); }
        }
    }</span></pre>
</div>
<p>这是一个简单的用户信息实体类，它继承了一个接口 IUser&nbsp; ，在App.config 中配置了数据连接后，就可以像下面这样使用了：</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;"><font color="#427d51">//注册并从容器中创建实体类</font>
<pre>EntityBuilder.RegisterType(<span style="color: #0000ff;">typeof</span>(IUser), <span style="color: #0000ff;">typeof</span><span style="color: #000000;">(UserEntity));
UserEntity user </span>= EntityBuilder.CreateEntity&lt;IUser&gt;() <span style="color: #0000ff;">as</span><span style="color: #000000;"> UserEntity;
</span><span style="color: #0000ff;"><font color="#427d51">//实体类作为索引器使用</font></span></pre>
<pre><span style="color: #0000ff;">bool</span> flag = (user[<span style="color: #800000;">"</span><span style="color: #800000;">User ID</span><span style="color: #800000;">"</span>] == <span style="color: #0000ff;">null</span>);<span style="color: #008000;">//</span><span style="color: #008000;">true

</span><span style="color: #008000;">//</span><span style="color: #008000;">删除测试数据</span>
LocalDbContext context = <span style="color: #0000ff;">new</span> LocalDbContext();<span style="color: #008000;">//</span><span style="color: #008000;">自动创建表</span>
OQL deleteQ =<span style="color: #000000;"> OQL.From(user)
    .Delete()
    .Where(cmp</span>=&gt;cmp.Comparer(user.UserID,<span style="color: #800000;">"</span><span style="color: #800000;">&gt;</span><span style="color: #800000;">"</span>,<span style="color: #800080;">0</span>)) <span style="color: #008000;">//</span><span style="color: #008000;">为了安全，不带Where条件是<font color="#ff0000">不会</font>全部删除数据的</span>
<span style="color: #000000;">    .END;
context.UserQuery.ExecuteOql(deleteQ);
Console.WriteLine(</span><span style="color: #800000;">"</span><span style="color: #800000;">插入3条测试数据</span><span style="color: #800000;">"</span><span style="color: #000000;">);
</span><span style="color: #008000;">//</span><span style="color: #008000;">插入几条测试数据</span>
context.Add&lt;UserEntity&gt;(<span style="color: #0000ff;">new</span> UserEntity() {  FirstName =<span style="color: #800000;">"</span><span style="color: #800000;">zhang</span><span style="color: #800000;">"</span>, LasttName=<span style="color: #800000;">"</span><span style="color: #800000;">san</span><span style="color: #800000;">"</span><span style="color: #000000;"> });
context.Add</span>&lt;IUser&gt;(<span style="color: #0000ff;">new</span> UserDto() { FirstName = <span style="color: #800000;">"</span><span style="color: #800000;">li</span><span style="color: #800000;">"</span>, LasttName = <span style="color: #800000;">"</span><span style="color: #800000;">si</span><span style="color: #800000;">"</span>, Age = <span style="color: #800080;">21</span><span style="color: #000000;"> });
context.Add</span>&lt;IUser&gt;(<span style="color: #0000ff;">new</span> UserEntity() { FirstName = <span style="color: #800000;">"</span><span style="color: #800000;">wang</span><span style="color: #800000;">"</span>, LasttName = <span style="color: #800000;">"</span><span style="color: #800000;">wu</span><span style="color: #800000;">"</span>, Age = <span style="color: #800080;">22</span><span style="color: #000000;"> });

</span><span style="color: #008000;">//</span><span style="color: #008000;">查找姓张的一个用户</span>
UserEntity uq = <span style="color: #0000ff;">new</span> UserEntity() { FirstName = <span style="color: #800000;">"</span><span style="color: #800000;">zhang</span><span style="color: #800000;">"</span><span style="color: #000000;"> };
OQL q </span>=<span style="color: #000000;"> OQL.From(uq)
   .Select(uq.UserID, uq.FirstName, uq.Age)
   .Where(uq.FirstName)
.END;

</span><span style="color: #008000;">//</span><span style="color: #008000;">下面的语句等效
</span><span style="color: #008000;">//</span><span style="color: #008000;">UserEntity user2 = EntityQuery&lt;UserEntity&gt;.QueryObject(q,context.CurrentDataBase);</span>
UserEntity user2 = context.UserQuery.GetObject(q);</pre>
</div>
<p>注意：该实例需要SOD框架最新版本的支持，你也可以使用之前的方式，使用EntityQuery&lt;T&gt; 来操作实体类。</p>
<p>另外，SOD的ORM也支持根据接口之间创建实体类并查询的功能，请看下面的示例：</p>
<div style="background-color: #f5f5f5; border: #cccccc 1px solid; padding: 5px;">
<pre><span style="color: #0000ff;"> static</span> <span style="color: #0000ff;">void</span><span style="color: #000000;"> TestGOQL()
 {
     </span><span style="color: #0000ff;">string</span> sqlInfo=<span style="color: #800000;">""</span><span style="color: #000000;">;
     </span><span style="color: #008000;">//</span><span style="color: #008000;">下面使用　ITable_User　或者　Table_User均可</span>
      List&lt;ITable_User&gt; userList =<span style="color: #000000;">
                OQL.FromObject</span>&lt;ITable_User&gt;<span style="color: #000000;">()
                </span><span style="color: #008000;">//</span><span style="color: #008000;">.Select() //选全部自断</span>
                .Select(s =&gt; <span style="color: #0000ff;">new</span> <span style="color: #0000ff;">object</span>[] { s.UID, s.Name, s.Sex }) <span style="color: #008000;">//</span><span style="color: #008000;">仅选取３个字段</span>
                  .Where((cmp, user) =&gt; cmp.Property(user.UID) &lt; <span style="color: #800080;">100</span><span style="color: #000000;">)
                .OrderBy((o,user)</span>=&gt;<span style="color: #000000;">o.Asc(user.UID))
                .Limit(</span><span style="color: #800080;">5</span>, <span style="color: #800080;">1</span>) <span style="color: #008000;">//</span><span style="color: #008000;">限制５条记录每页，取第一页</span>
                .Print(<span style="color: #0000ff;">out</span><span style="color: #000000;"> sqlInfo)
              .ToList();

            Console.WriteLine(sqlInfo);
            Console.WriteLine(</span><span style="color: #800000;">"</span><span style="color: #800000;">User List item count:{0}</span><span style="color: #800000;">"</span><span style="color: #000000;">,userList.Count);
        }</span></pre>
</div>
<p>有关该功能的详细内容介绍，请看博客文章《<a href="http://www.cnblogs.com/bluedoctor/p/3272993.html">一行代码调用实现带字段选取＋条件判断＋排序＋分页功能的增强ORM框架</a>》。</p>
<h1>六、Data Control</h1>
<p>框架支持Windows Forms，WebForms 的窗体编程，扩展了一套数据控件，包括常用的 文本框、复选框、单选框、列表框、日历控件、标签控件等。这些控件全部遵循SOD的窗体数据接口，实现这个接口的控件将极大的简化窗体应用程序的数据操作，有关内容详细介绍，请看这篇博客文章：《<a href="http://www.cnblogs.com/bluedoctor/archive/2013/03/28/2986580.html">不使用反射，&ldquo;一行代码&rdquo;实现Web、WinForm窗体表单数据的填充、收集、清除，和到数据库的CRUD</a>》</p>
<p>你也可以在 pwmis.codeplex.com 下载源码，找到下面地址对应的<a href="http://pwmis.codeplex.com/SourceControl/latest#SOD/Example/SimpleAccessWinForm/SimpleAccessWinForm-2013.csproj" target="_blank">SimpleAccessWinForm</a>，</p>
<p>或者下载这个 <img src="http://download-codeplex.sec.s-msft.com/Images/v20959/Example.gif" alt="Example" /> <a href="http://pwmis.codeplex.com/downloads/get/645428">PDF.Net_V4.6 WinForm 数据表单实例</a></p>
<p>&nbsp;</p>
<p>或者看这个 <img src="http://download-codeplex.sec.s-msft.com/Images/v20959/SourceCode.gif" alt="Source Code" /> <a href="http://pwmis.codeplex.com/downloads/get/806244">WebTestTool</a></p>
<p>这2个示例应用程序，都演示了WinForm下如何使用Data Control 数据控件简化CRUD窗体编程。</p>
<p>另外，如果你是WebForms 应用程序，开源项目的<a href="http://pwmis.codeplex.com/SourceControl/latest#SOD/PWMIS-2013.sln" target="_blank">超市管理系统源码</a> 你可以看看，</p>
<p>或者直接下载这个（版本较老）<img src="http://download-codeplex.sec.s-msft.com/Images/v20959/SourceCode.gif" alt="Source Code" /> <a href="http://pwmis.codeplex.com/downloads/get/644978">PDF.Net_V4.6_OpenSource (new)</a></p>
<p><br /> &nbsp;</p>
<h1>结束语：</h1>
<p><strong>SOD不仅仅是一个ORM</strong>，它还有SQL-MAP和DataControl，具体可以看框架官网 <a href="http://www.pwmis.com/sqlmap"> http://www.pwmis.com/sqlmap</a> ，9年历史铸就的成果，坚固可靠。</p>
<p>非常感谢你看到这里，相信你初步了解了SOD框架的基本功能，如果您还有其它问题，欢迎你在项目的开源网站 pwmis.codeplex.com 的讨论去发帖，或者去官方博客相关文章回帖也可。</p>
<p>&nbsp;</p>
<p>最后，祝愿所有.NET 程序员早日摆脱日复一日的CRUD功能，感谢大家对PDF.NET SOD框架一如既往的支持，</p>
<p><font size="5" color="#c0504d">2015年新春之际，祝贺各位会员朋友喜气洋洋，合家欢乐，万事如意！</font></p>
<p>&nbsp;</p>
<p>附注：如果大家还没有买到节日期间的火车票，推荐本框架开发作者自主开发的《<a href="http://pwmis.codeplex.com/releases/view/117822" target="_blank">12306无声购票弹窗通知小工具</a>》，工作抢票2不误，绿色无毒安全放心！</p>
<p>&nbsp;</p>
<p>深蓝医生</p>
<p>2015.2 月春节</p>